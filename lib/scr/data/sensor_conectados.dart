// To parse this JSON data, do
//
//     final sensorc = sensorcFromJson(jsonString);

import 'dart:convert';

List<Sensorc> sensorcFromJson(String str) =>
    List<Sensorc>.from(json.decode(str).map((x) => Sensorc.fromJson(x)));

String sensorcToJson(List<Sensorc> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Sensorc {
  Sensorc({
    required this.id,
    required this.temp,
    required this.hum,
    required this.estado,
    required this.ultimoRegistro,
  });

  String id;
  String temp;
  String hum;
  bool estado;
  DateTime ultimoRegistro;

  factory Sensorc.fromJson(Map<String, dynamic> json) => Sensorc(
        id: json["id"],
        temp: json["temp"],
        hum: json["hum"],
        estado: json["estado"],
        ultimoRegistro: DateTime.parse(json["ultimoRegistro"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "temp": temp,
        "hum": hum,
        "estado": estado,
        "ultimoRegistro": ultimoRegistro.toIso8601String(),
      };
}
