// To parse this JSON data, do
//
//     final sensor = sensorFromJson(jsonString);

import 'dart:convert';

List<Sensor> sensorFromJson(String str) =>
    List<Sensor>.from(json.decode(str).map((x) => Sensor.fromJson(x)));

String sensorToJson(List<Sensor> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Sensor {
  Sensor({
    required this.temperatura,
    required this.createdAt,
    required this.humedad,
    required this.clienteId,
  });

  String temperatura;
  DateTime createdAt;
  String humedad;
  String clienteId;

  factory Sensor.fromJson(Map<String, dynamic> json) => Sensor(
        temperatura: json["temp"],
        createdAt: DateTime.parse(json["time"]),
        humedad: json["hum"],
        clienteId: json["idDispositivo"],
      );

  Map<String, dynamic> toJson() => {
        "temp": temperatura,
        "time": createdAt.toIso8601String(),
        "hum": humedad,
        "idDispositivo": clienteId,
      };
}
