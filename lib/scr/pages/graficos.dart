// ignore_for_file: , prefer_const_constructors, prefer_const_literals_to_create_immutables, sort_child_properties_last, prefer_interpolation_to_compose_strings, prefer_const_constructors_in_immutables, avoid_unnecessary_containers

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sensor/scr/data/sensor.dart';
import 'package:sensor/scr/provider/remote.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class GrafPage extends StatefulWidget {
  final String text;
  GrafPage({Key? key, required this.text}) : super(key: key);

  @override
  GrafPageState createState() => GrafPageState();
}

class GrafPageState extends State<GrafPage> {
  List<Sensor>? opcionesDias;
  List<Sensor>? opcionesSemanas;
  List<Sensor>? opcionesMeses;
  var isLoaded = false;
  @override
  void initState() {
    super.initState();
    cargarData();
  }

  cargarData() async {
    //si no hay datos del dia dara nulo
    //opcionesDias = await RemoteService().getDataRegistrosDias(widget.text);
    opcionesSemanas = await RemoteService().getDataRegistrosSemana(widget.text);
    opcionesMeses = await RemoteService().getDataRegistrosMes(widget.text);
    if (opcionesDias != null) {
      setState(() {
        isLoaded = true;
      });
    }
    if (opcionesSemanas != null) {
      setState(() {
        isLoaded = true;
      });
    }
    if (opcionesMeses != null) {
      setState(() {
        isLoaded = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    //DIA
    int dias = 0;
    if (opcionesSemanas != null) {
      dias = opcionesSemanas!.length;
    } else {
      dias = 0; //return value if str is null
    }
    //Llenamos la lista para el grafico
    final List<TempData> chartDataDias = [
      for (int i = 0; i < dias; i++)
        TempData(opcionesSemanas![i].createdAt,
            double.parse(opcionesSemanas![i].temperatura))
    ];
    //SEMANA
    int semana = 0;
    if (opcionesSemanas != null) {
      semana = opcionesSemanas!.length;
    } else {
      semana = 0; //return value if str is null
    }
    //Llenamos la lista para el grafico
    final List<TempData> chartDataSemana = [
      for (int i = 0; i < semana; i++)
        TempData(opcionesSemanas![i].createdAt,
            double.parse(opcionesSemanas![i].temperatura))
    ];
    //MES
    int mes = 0;
    if (opcionesMeses != null) {
      mes = opcionesMeses!.length;
    } else {
      mes = 0; //return value if str is null
    }
    //Llenamos la lista para el grafico
    final List<TempData> chartDataMes = [
      for (int i = 0; i < mes; i++)
        TempData(opcionesMeses![i].createdAt,
            double.parse(opcionesMeses![i].temperatura))
    ];
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Color(0xff1976d2),
            //backgroundColor: Color(0xff308e1c),
            bottom: TabBar(
              indicatorColor: Color(0xff9962D0),
              tabs: [
                Tab(text: ('Diario')),
                Tab(text: ('Semana')),
                Tab(text: ('Mensual')),
                Tab(text: ('Regresar')),
              ],
            ),
            title: Text('Historial'),
          ),
          body: TabBarView(
            children: [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Container(
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Text(
                          'Registro del dia',
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold),
                        ),
                        Expanded(
                            child: SfCartesianChart(
                                primaryXAxis: DateTimeAxis(),
                                series: <ChartSeries>[
                              // Renders line chart
                              LineSeries<TempData, DateTime>(
                                  dataSource: chartDataDias,
                                  xValueMapper: (TempData sales, _) =>
                                      sales.year,
                                  yValueMapper: (TempData sales, _) =>
                                      sales.temp)
                            ])),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Container(
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Text(
                          'Registros de la semana',
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Expanded(
                          child: SfCartesianChart(
                              primaryXAxis: DateTimeAxis(),
                              series: <ChartSeries>[
                                // Renders line chart
                                LineSeries<TempData, DateTime>(
                                    dataSource: chartDataSemana,
                                    xValueMapper: (TempData sales, _) =>
                                        sales.year,
                                    yValueMapper: (TempData sales, _) =>
                                        sales.temp)
                              ]),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Container(
                  child: Center(
                    child: Column(
                      children: <Widget>[
                        Text(
                          'Registro del mes',
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold),
                        ),
                        Expanded(
                            child: SfCartesianChart(
                                primaryXAxis: DateTimeAxis(),
                                series: <ChartSeries>[
                              // Renders line chart
                              LineSeries<TempData, DateTime>(
                                  dataSource: chartDataMes,
                                  xValueMapper: (TempData sales, _) =>
                                      sales.year,
                                  yValueMapper: (TempData sales, _) =>
                                      sales.temp)
                            ])),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Container(
                  child: Center(
                    child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.symmetric(
                              horizontal: 24, vertical: 12),
                          textStyle: TextStyle(fontSize: 12),
                        ),
                        child: Text('Regresar'),
                        onPressed: () => Navigator.of(context).pop()),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TempData {
  TempData(this.year, this.temp);
  final DateTime year;
  final double temp;
}
