// ignore_for_file: use_key_in_widget_constructors, prefer_const_constructors, prefer_const_literals_to_create_immutables, sort_child_properties_last, prefer_interpolation_to_compose_strings

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
//import 'package:sensor/scr/data/sensor_conectados.dart';

import 'package:sensor/scr/data/sensor_conectados.dart';
import 'package:sensor/scr/pages/graficos.dart';
import 'package:sensor/scr/provider/remote.dart';

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  //Creamos la lista de la clase Sensorc
  List<Sensorc>? opciones;
  var isLoaded = false;

  @override
  void initState() {
    super.initState();
    cargarData();
  }

  cargarData() async {
    //Creamos la llenamos la lista con los datos de la api
    opciones = await RemoteService().getDataConectados();
    if (opciones != null) {
      setState(() {
        isLoaded = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Sensores"),
      ),
      body: Visibility(
        visible: isLoaded,
        child: ListView.builder(
          //Se muestra un registro de datos a la vez
          itemCount: opciones?.length,
          itemBuilder: (BuildContext context, int index) {
            var dateString = DateFormat('yyyy-MM-dd – kk:mm:ss')
                .format(opciones![index].ultimoRegistro);
            bool estado = opciones![index].estado;
            String id = opciones![index].id;
            return ListTile(
              title: Card(
                  elevation: 5.0,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: Row(
                    children: [
                      Column(
                        //Primero muestra el id
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Id Cliente:   ' + opciones![index].id,
                            style: TextStyle(fontSize: 25),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            //Abajo de id la temperatura y humedad, uno al lado del otro
                            children: [
                              Text('Temperatura: ${opciones![index].temp}   ',
                                  style: TextStyle(fontSize: 15)),
                              Text('Humedad: ${opciones![index].hum}',
                                  style: TextStyle(fontSize: 15)),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          //Abajo el tiempo
                          Text('Ultimo registro: $dateString',
                              style: TextStyle(fontSize: 15)),
                        ],
                      ),
                      Expanded(
                          child: ListTile(
                              trailing: (() {
                        if (estado == true) {
                          return Icon(Icons.signal_wifi_4_bar,
                              color: Colors.green, size: 50);
                        } else {
                          return Icon(Icons.signal_wifi_connected_no_internet_4,
                              color: Colors.grey, size: 50);
                        }
                      }()))),
                    ],
                  )),
              onTap: () async {
                //A la tarjeta que toquemos le pasaremos el id a la siguiente pagina
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => GrafPage(text: id)));
              },
            );
          },
        ),
        replacement: const Center(child: CircularProgressIndicator()),
      ),
    );
  }
}
