// ignore_for_file: body_might_complete_normally_nullable

import 'package:sensor/scr/data/sensor.dart';
import 'package:sensor/scr/data/sensor_conectados.dart';
import 'package:http/http.dart' as http;

class RemoteService {
  Future<List<Sensor>?> getDataRegistrosDias(String text) async {
    var client = http.Client();

    var uri =
        Uri.parse('https://api-sensor-elc105.herokuapp.com/sensordata/$text');

    var resp = await client.get(uri);
    if (resp.statusCode == 200) {
      var json = resp.body;
      return sensorFromJson(json);
    }
  }

  Future<List<Sensor>?> getDataRegistrosSemana(String text) async {
    var client = http.Client();

    var uri = Uri.parse(
        'https://api-sensor-elc105.herokuapp.com/getHistorialPorSemana/$text');

    var resp = await client.get(uri);
    if (resp.statusCode == 200) {
      var json = resp.body;
      return sensorFromJson(json);
    }
  }

  Future<List<Sensor>?> getDataRegistrosMes(String text) async {
    var client = http.Client();

    var uri = Uri.parse(
        'https://api-sensor-elc105.herokuapp.com/getHistorialPorMes/$text');

    var resp = await client.get(uri);
    if (resp.statusCode == 200) {
      var json = resp.body;
      return sensorFromJson(json);
    }
  }

  Future<List<Sensorc>?> getDataConectados() async {
    var client = http.Client();

    var uri =
        Uri.parse('https://api-sensor-elc105.herokuapp.com/getDispositivos');

    var resp = await client.get(uri);
    if (resp.statusCode == 200) {
      var json = resp.body;
      return sensorcFromJson(json);
    }
  }
}
