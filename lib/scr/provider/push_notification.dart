import 'dart:async';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

class PushNotifications {
  static FirebaseMessaging messaging = FirebaseMessaging.instance;
  static String? token;

  //Controlador
  static final StreamController<String> _messageString =
      StreamController.broadcast();
  //Agarramos el mensaje
  static Stream<String> get messageString => _messageString.stream;

  //Aplicacion abierta
  static Future _onBackgroudHandler(RemoteMessage message) async {
    debugPrint(message.notification!.body.toString());
    _messageString.add(message.notification!.body.toString());
  }

  //Aplicacion abierta
  static Future _onMessageHandler(RemoteMessage message) async {
    debugPrint(message.notification!.body.toString());
    _messageString.add(message.notification!.body.toString());
  }

  //Aplicacion en segundo plano
  static Future _onMessageOpenApp(RemoteMessage message) async {
    debugPrint(message.notification!.body.toString());
    _messageString.add(message.notification!.body.toString());
  }

  //lanzador
  static Future initializeApp() async {
    await Firebase.initializeApp();
    token = await FirebaseMessaging.instance.getToken();
    debugPrint('Token ----> $token');
    FirebaseMessaging.onBackgroundMessage(_onBackgroudHandler);
    FirebaseMessaging.onMessage.listen(_onMessageHandler);
    FirebaseMessaging.onMessageOpenedApp.listen(_onMessageOpenApp);
  }

  static closeContorller() => _messageString.close();
}
