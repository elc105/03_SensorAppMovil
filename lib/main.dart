// ignore_for_file: avoid_print, prefer_const_constructors_in_immutables
/*
import 'package:firebase_messaging/firebase_messaging.dart';
import 'firebase_options.dart';
import 'package:firebase_core/firebase_core.dart';

*/
import 'package:flutter/material.dart';

import 'package:sensor/scr/provider/push_notification.dart';

import 'package:sensor/scr/pages/home_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //Llama al metodo initializeApp en la clase PushNotifications
  await PushNotifications.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey();
  final GlobalKey<ScaffoldMessengerState> scatorKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    debugPrint('estado');
    //Cuando llega una nueva notificacion refresca la pagina principal
    PushNotifications.messageString.listen((message) {
      navigatorKey.currentState?.pop(context);
      navigatorKey.currentState?.pushNamed("home", arguments: message);

      //Manda una notificacion normal
      final snackBar = SnackBar(
        content: Text(message),
        backgroundColor: Colors.black,
      );
      scatorKey.currentState?.showSnackBar(snackBar);
      debugPrint('mensaje');
    });
  }

  @override
  Widget build(context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: "home",
      navigatorKey: navigatorKey,
      scaffoldMessengerKey: scatorKey,
      routes: {
        "home": (_) => HomePage(),
      },
    );
  }
}



/*
errores
PlatformException (PlatformException(channel-error, Unable to establish connection on channel., null, null))
flutter pub outdated

*/